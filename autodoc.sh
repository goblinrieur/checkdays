#! /usr/bin/env bash
# -*- coding: UTF8 -*-

if [ $# -lt 1 ]; then
	echo "missing argument (filename)"
	exit 1
fi

# manage .fs files as gforth
if [[ "$1" =~ ".fs" ]] ; then 
	echo "gfroth ?"
	grep -iE '\\ |^: ' "$1" > "$1".autodoc.doc
	exit 0
fi

# manage .sh files as shell scripts
if [[ "$1" =~ ".sh" ]] ; then 
	echo "bash ?"
	grep -iE 'function|^#|.* #.*$' "$1" | grep -viE 'grep|if \[' > "$1".autodoc.doc
	exit 0
fi

# manage .py files as python scripts
if [[ "$1" =~ ".py" ]] ; then 
	echo "python ?"
	grep -iE 'class|def|^#|.* #.*$' "$1" | grep -viE 'grep|if \[' > "$1".autodoc.doc
	exit 0
fi
