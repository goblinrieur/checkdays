#! /usr/bin/env bash
# -*- coding: UTF8 -*-

docker build -t francoispussault/calendar:latest .

if [ $? -eq 0 ]; then 
		docker run -it francoispussault/calendar:latest || echo FAILED
fi

exit $?
