# check-days

Defines specific days and other calendar functions test (gforth)

Apply an example code of exploitation of that functions set

So from a user input calculte & display the weekday it was or will be in range of year 1752 to year 2050. This limitation is du to the calculation method itself.

# run example 
![example](./example.png)

# current code 

[current_code](./get_day_from_date.fs)

[current_doc](./get_day_from_date.fs.autodoc.doc)

# W.I.P.

- [X] complete that with existing main & optimisation branches

- [X] tag it as v0.3

- [X] tag it as v0.4 with colors/cursor/exit state management

- [X] merge to main version

# can dockerize it 

```
./dockerbuild.sh

Sending build context to Docker daemon    194kB
Step 1/5 : FROM debian:latest
 ---> c69ac6bed067
Step 2/5 : RUN apt -y update && apt install -y gforth --fix-missing
 ---> Using cache
 ---> d53fb07b8095
Step 3/5 : COPY get_day_from_date.fs .
 ---> Using cache
 ---> 0332fbdb955f
Step 4/5 : COPY dayofweek.sh .
 ---> Using cache
 ---> ad1483b78831
Step 5/5 : ENTRYPOINT ./dayofweek.sh
 ---> Using cache
 ---> e8ff2acbf2e4
Successfully built e8ff2acbf2e4
Successfully tagged francoispussault/calendar:latest

 Day     ? 9
 Month   ? 11
 Year    ? 1976

     Tuesday


```

