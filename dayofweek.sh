#!  /usr/bin/env bash

if [ $(which gforth | wc -c ) -lt 5 ] ; then
        echo -e "\e[0;31m gforth not found\e[0m"
        exit 1
else
    $(which gforth) ./get_day_from_date.fs
fi
exit 0
