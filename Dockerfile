FROM debian:latest
RUN apt -y update && apt install -y gforth --fix-missing
COPY get_day_from_date.fs .
COPY dayofweek.sh .
ENTRYPOINT ./dayofweek.sh
